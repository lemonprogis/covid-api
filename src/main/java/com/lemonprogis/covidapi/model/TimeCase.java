package com.lemonprogis.covidapi.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class TimeCase {
    private String countryRegion;
    private LocalDateTime lastUpdate;
    private int confirmed;
    private int deaths;
    private int recovered;
    private int active;
    private int deltaConfirmed;
    private int deltaRecovered;
}
