package com.lemonprogis.covidapi.model;

import lombok.Data;

@Data
public class NotFound {
    private String county;
    private String provinceState;
    private String countryRegion;
    private int confirmed;
    private int deaths;
}
