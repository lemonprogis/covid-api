package com.lemonprogis.covidapi.model;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class StateCase {
    private int fips;
    private String provinceState;
    private String countryRegion;
    private LocalDateTime lastUpdate;
    private double latitude;
    private double longitude;
    private int confirmed;
    private int deaths;
    private int recovered;
    private int active;
}
